# Messenger-Notify

__DEPRECATED__: Use the [Flare](https://gitlab.com/schmiddi-on-mobile/flare) application and turn on running in the background and notifications. Regarding Matrix, I currently see no viable alternative, but for me, the Matrix backend used for messenger-notify constantly crashed after some updates.

A background service that can be connected to [Signal](https://signal.org/) or [Matrix](https://matrix.org/) and will show a desktop notification on message receive.

## Usage

Compile the application (assuming you have [Rust](https://www.rust-lang.org/) installed):

```
cargo build --release
```

You can also choose to compile it with only some messengers (for all possible messengers, see the `features`-section in `Cargo.toml`) enabled using e.g.:

```
cargo build --release --no-default-feature --features signal
```

The binary will be located in `target/release/messenger-notify`. Optionally, move it into `$PATH`.  Before the first run, link your desired accounts (otherwise, `messenger-notify` will just exit and do nothing). Restart `messenger-notify` with no arguments, everything should work fine. You can test it by for example sending a "Note to self" in the Signal application, your note should pop up as a notification.

### Signal

Execute the script with `messenger-notify --link-signal <device-name>`, where `<device-name>` is the desired device name you want in the official signal app. A QR-code will be shown that can be used to link the device to the official Signal application on your phone. Scan it and grant access to the application. After successful linking, the program will exit.

### Matrix

Execute the script with `messenger-notify --link-matrix <user-id>` (user-id should be of the form `@username:server.url`) and type in the password in the prompt that should be shown (the password you type out will not be shown). After successful linking, the program will exit.

## Auto-start

In the repository, the `messenger-notify.service` can be used to auto-start the daemon using systemd as a user service. 
To use it copy the file to `~/.config/systemd/user/` (on Arch Linux and derivatives, might vary depending on the distribution) and start the service using `systemctl --user enable --now messenger-notify`. Note that this service assumes your binary to be somewhere in the `$PATH`. Also make sure to run the binary at least once with `link-signal` so you can link it. 

## Configuration

The default configuration file can be inspected in `config.toml`. You should copy this file to `~/.config/messenger-notify/config.toml` if you want to modify and values.

## Cross-Compilation

If you intent to use this application on the PinePhone, it can take ages (~2 hours) with relatively high memory and CPU usage (you will not be able to do anything during that time). Instead, you can also cross-compile the application. Here are the steps to do that:

- Install [cross](https://github.com/cross-rs/cross) (including the dependencies like docker, starting docker, ...).
- Uncomment the line `openssl = ...` in the `Cargo.toml`.
- Run `cross build --release --target aarch64-unknown-linux-gnu`.
- Move the resulting `target/aarch64-unknown-linux-gnu/release/messenger-notify` to your PinePhone.

## Security

This application does, to the best of my knowledge, neither store messages on the disk nor keep them in memory longer than needed to send the notification. Regardless, this application will likely worsen the security compared to official Signal/Matrix products. Use this application with care when handling sensitive data.
