#![cfg_attr(feature = "matrix", allow(dead_code))]
use notify_rust::Notification;

use crate::{config::Config, notification::IntoNotification};

#[non_exhaustive]
#[derive(Debug)]
pub enum Messenger {
    #[cfg(feature = "signal")]
    Signal,
    #[cfg(feature = "matrix")]
    Matrix,
}

impl Messenger {
    fn to_icon<'a>(&self, config: &'a Config) -> &'a str {
        match self {
            #[cfg(feature = "signal")]
            Messenger::Signal => &config.signal.icon,
            #[cfg(feature = "matrix")]
            Messenger::Matrix => &config.matrix.icon,
        }
    }
}

#[derive(Debug)]
pub enum Message {
    Call(CallMessage),
    Text(TextMessage),
}

#[derive(Debug)]
pub struct CallMessage {
    pub messenger: Messenger,
    pub timestamp: u64,
    pub sender: String,
    pub sender_id: String,
    pub r#type: CallType,
}

#[derive(Debug)]
pub enum CallType {
    Audio,
    Video,
}

#[derive(Debug)]
pub struct TextMessage {
    pub messenger: Messenger,
    pub timestamp: u64,
    pub sender: String,
    pub sender_id: String,
    pub group: Option<String>,
    pub body: Option<String>,
    pub reaction: Option<String>,
    pub sticker: bool,
    pub attachments: Vec<Attachment>,
}

#[derive(Debug)]
pub enum Attachment {
    Audio,
    Image,
    Video,
    File,
}

#[derive(Debug, Hash, Eq, PartialEq, Clone)]
pub struct MessageIden {
    pub timestamp: u64,
    pub sender: String,
}

impl std::fmt::Display for Attachment {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::Audio => write!(f, "audio"),
            Self::Image => write!(f, "image"),
            Self::Video => write!(f, "video"),
            Self::File => write!(f, "file"),
        }
    }
}

impl IntoNotification for Message {
    fn as_notification(&self, config: &Config) -> Notification {
        match self {
            Message::Call(m) => m.as_notification(config),
            Message::Text(m) => m.as_notification(config),
        }
    }
}

impl Message {
    pub fn sender(&self) -> &String {
        match self {
            Message::Call(m) => &m.sender,
            Message::Text(m) => &m.sender,
        }
    }
}

impl TextMessage {
    fn summary(&self) -> String {
        if let Some(group) = &self.group {
            format!("{} ({})", self.sender, group)
        } else {
            self.sender.clone()
        }
    }

    fn body(&self) -> String {
        let mut parts = vec![];
        if let Some(body) = &self.body {
            parts.push(body.clone());
        }
        if let Some(reaction) = &self.reaction {
            parts.push(format!("Reacted {}", reaction));
        }
        if self.sticker {
            parts.push("Sticker".to_owned());
        }
        for att in &self.attachments {
            parts.push(format!("Sent {}", att));
        }

        parts.join(", ")
    }
}

impl IntoNotification for TextMessage {
    fn as_notification(&self, config: &Config) -> Notification {
        Notification::new()
            .summary(&self.summary())
            .body(&self.body())
            .icon(self.messenger.to_icon(config))
            .clone()
    }
}

impl IntoNotification for CallMessage {
    fn as_notification(&self, config: &Config) -> Notification {
        Notification::new()
            .summary(&self.sender)
            .body(match self.r#type {
                CallType::Audio => "Is audio-calling",
                CallType::Video => "Is video-calling",
            })
            .icon(self.messenger.to_icon(config))
            .clone()
    }
}

impl From<&Message> for MessageIden {
    fn from(msg: &Message) -> MessageIden {
        match msg {
            Message::Call(m) => MessageIden {
                timestamp: m.timestamp,
                sender: m.sender_id.clone(),
            },
            Message::Text(m) => MessageIden {
                timestamp: m.timestamp,
                sender: m.sender_id.clone(),
            },
        }
    }
}
