#[derive(Debug)]
pub enum Error {
    Notification(notify_rust::error::Error),
    #[cfg(feature = "signal")]
    Presage(presage::Error),
    IO(std::io::Error),
    Toml(toml::de::Error),
    Dbus(zbus::Error),
    #[cfg(feature = "matrix")]
    Json(serde_json::Error),
    #[cfg(feature = "matrix")]
    Matrix(matrix_sdk::Error),
    #[cfg(feature = "matrix")]
    MatrixClientBuild(matrix_sdk::ClientBuildError),
    #[cfg(feature = "matrix")]
    MatrixOpenStore(matrix_sdk::store::OpenStoreError),
    #[cfg(feature = "matrix")]
    RumaIdParse(matrix_sdk::ruma::IdParseError),
}

impl From<notify_rust::error::Error> for Error {
    fn from(e: notify_rust::error::Error) -> Self {
        Self::Notification(e)
    }
}

#[cfg(feature = "signal")]
impl From<presage::Error> for Error {
    fn from(e: presage::Error) -> Self {
        Self::Presage(e)
    }
}

impl From<std::io::Error> for Error {
    fn from(e: std::io::Error) -> Self {
        Self::IO(e)
    }
}

impl From<toml::de::Error> for Error {
    fn from(e: toml::de::Error) -> Self {
        Self::Toml(e)
    }
}

impl From<zbus::Error> for Error {
    fn from(e: zbus::Error) -> Self {
        Self::Dbus(e)
    }
}

#[cfg(feature = "matrix")]
impl From<serde_json::Error> for Error {
    fn from(e: serde_json::Error) -> Self {
        Self::Json(e)
    }
}

#[cfg(feature = "matrix")]
impl From<matrix_sdk::Error> for Error {
    fn from(e: matrix_sdk::Error) -> Self {
        Self::Matrix(e)
    }
}

#[cfg(feature = "matrix")]
impl From<matrix_sdk::ClientBuildError> for Error {
    fn from(e: matrix_sdk::ClientBuildError) -> Self {
        Self::MatrixClientBuild(e)
    }
}

#[cfg(feature = "matrix")]
impl From<matrix_sdk::store::OpenStoreError> for Error {
    fn from(e: matrix_sdk::store::OpenStoreError) -> Self {
        Self::MatrixOpenStore(e)
    }
}

#[cfg(feature = "matrix")]
impl From<matrix_sdk::ruma::IdParseError> for Error {
    fn from(e: matrix_sdk::ruma::IdParseError) -> Self {
        Self::RumaIdParse(e)
    }
}

impl std::fmt::Display for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Error::Notification(e) => writeln!(f, "Failed to send notification: {}", e),
            #[cfg(feature = "signal")]
            Error::Presage(e) => writeln!(f, "Signal manager failed: {}", e),
            Error::IO(e) => writeln!(f, "IO error: {}", e),
            Error::Toml(e) => writeln!(f, "Configuration file error: {}", e),
            Error::Dbus(e) => writeln!(f, "Dbus error: {}", e),
            #[cfg(feature = "matrix")]
            Error::Json(e) => writeln!(f, "Json error: {}", e),
            #[cfg(feature = "matrix")]
            Error::Matrix(e) => writeln!(f, "Matrix error: {}", e),
            #[cfg(feature = "matrix")]
            Error::MatrixClientBuild(e) => writeln!(f, "Matrix client build error: {}", e),
            #[cfg(feature = "matrix")]
            Error::MatrixOpenStore(e) => writeln!(f, "Matrix open store build error: {}", e),
            #[cfg(feature = "matrix")]
            Error::RumaIdParse(e) => writeln!(f, "Ruma ID parse error: {}", e),
        }
    }
}

impl std::error::Error for Error {}
