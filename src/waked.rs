use std::time::{SystemTime, UNIX_EPOCH};

use zbus::{dbus_proxy, Connection};

#[dbus_proxy(
    interface = "de.seath.Waked",
    default_service = "de.seath.Waked",
    default_path = "/de/seath/Waked/Alarm"
)]
trait Wake {
    fn add(&self, id: &str, time: u64) -> zbus::Result<String>;
}

pub struct Waker {
    connection: Connection,
}

impl Waker {
    pub async fn new() -> Result<Self, zbus::Error> {
        Ok(Self {
            connection: Connection::system().await?,
        })
    }
    pub async fn schedule_wakeup(&self, time: SystemTime) -> Result<(), zbus::Error> {
        let unix = time
            .duration_since(UNIX_EPOCH)
            .expect("Time went backwards")
            .as_secs();
        log::trace!("Schedule next wakeup to be at {}", unix);

        let proxy = WakeProxy::new(&self.connection).await?;
        let _ = proxy
            .add(
                &format!("de.schmidhuberj.SignalNotify-{}", unix),
                unix
            )
            .await?;

        Ok(())
    }
}
