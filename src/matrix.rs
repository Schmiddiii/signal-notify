use std::{
    fmt::Write,
    fs::File,
    path::{Path, PathBuf},
};

use matrix_sdk::{
    config::SyncSettings,
    encryption::verification::{SasVerification, Verification},
    instant::Duration,
    room::Room,
    ruma::{
        api::client::push::get_notifications::v3::Notification,
        events::{
            room::message::{MessageType, TextMessageEventContent},
            AnySyncMessageLikeEvent, AnySyncRoomEvent, AnyToDeviceEvent, SyncMessageLikeEvent,
        },
        UserId,
    },
    store::make_store_config,
    Client, LoopCtrl, Session,
};
use tokio::sync::{mpsc::Sender, watch};

use crate::{
    config::MatrixConfig,
    message::{Message, MessageIden, TextMessage},
    notification::confirm_notification,
    Error, YOU,
};

pub struct Matrix {
    client: Option<Client>,
    path: PathBuf,
    config: MatrixConfig,
}

impl Matrix {
    pub async fn new<P: AsRef<Path>>(path: P, config: MatrixConfig) -> Result<Self, Error> {
        log::trace!("Creating new matrix instance");
        let path = path.as_ref().to_owned();
        let store = make_store_config(&path, None)?;
        let mut session_file = path.clone();
        session_file.push("session.json");
        let client = if session_file.exists() && session_file.is_file() {
            log::trace!("There already exists a session file, restoring login");
            let file = File::open(&session_file)?;
            let session: Session = serde_json::from_reader(file)?;
            let client = Client::builder()
                .store_config(store)
                .user_id(&session.user_id)
                .build()
                .await?;
            client.restore_login(session).await?;
            Some(client)
        } else {
            log::trace!("There does not seem to be a session file, do not build client");
            None
        };
        Ok(Self {
            client,
            path,
            config,
        })
    }

    pub async fn login<S: AsRef<str>>(&mut self, user_id: S, password: S) -> Result<(), Error> {
        log::trace!("Logging in matrix");
        let store = make_store_config(&self.path, None)?;
        let user_id = user_id.as_ref().to_owned();
        let client = Client::builder()
            .store_config(store)
            .user_id(&UserId::parse(&user_id.clone())?)
            .build()
            .await?;
        client
            .login(&user_id, password.as_ref(), None, None)
            .await?;
        self.client = Some(client);

        log::trace!("Writing the session file");
        let mut session_file = self.path.clone();
        session_file.push("session.json");
        let file = if session_file.exists() {
            File::open(session_file)
        } else {
            File::create(session_file)
        }?;
        if let Some(session) = self.client.as_ref().unwrap().session().await {
            serde_json::to_writer(file, &session)?;
        }
        Ok(())
    }

    async fn on_notification(
        n: Notification,
        room: Room,
        client: Client,
        message_sender: Sender<Message>,
        config: MatrixConfig,
    ) {
        log::trace!("Matrix server sent a notification");
        if let AnySyncRoomEvent::MessageLike(AnySyncMessageLikeEvent::RoomMessage(
            SyncMessageLikeEvent::Original(event),
        )) = n
            .event
            .deserialize()
            .expect("Failed to deserialize notification")
        {
            let mut skip = false;

            let msg_body = match event.content.msgtype {
                MessageType::Text(TextMessageEventContent { ref body, .. }) => body.clone(),
                _ => return,
            };
            let user_id = client.user_id().await;
            let sender = if Some(event.sender.clone()) == user_id {
                YOU.to_owned()
            } else {
                room.get_member(&event.sender)
                    .await
                    .ok()
                    .flatten()
                    .and_then(|m| m.display_name().map(|s| s.to_owned()))
                    .unwrap_or_else(|| event.sender.as_str().to_owned())
            };
            let room_name = room.name();
            if room_name.is_none()
                || config.skip_rooms.contains(room_name.as_ref().unwrap())
                || !config.only_rooms.is_empty()
                    && !config.only_rooms.contains(room_name.as_ref().unwrap())
            {
                // Skip this message
                skip = true;
            }

            if let MessageType::Text(TextMessageEventContent { formatted, .. }) =
                event.content.msgtype
            {
                if config.all_mentions
                    && formatted.is_some()
                    && formatted
                        .unwrap()
                        .body
                        .contains(&user_id.map(|id| id.to_string()).unwrap_or_default())
                {
                    skip = false;
                }
            };

            if config.all_private && room.members().await.unwrap_or_default().len() <= 2 {
                skip = false;
            }

            if skip {
                return;
            }
            let msg = TextMessage {
                messenger: crate::message::Messenger::Matrix,
                timestamp: event.origin_server_ts.get().into(),
                sender,
                sender_id: event.sender.as_str().to_owned(),
                group: room_name,
                body: Some(msg_body),
                // TODO
                reaction: None,
                sticker: false,
                attachments: vec![],
            };
            message_sender
                .send(Message::Text(msg))
                .await
                .expect("Matrix failed to send message through pipe");
        }
    }

    pub async fn receive(
        &self,
        sender: Sender<Message>,
        _sender_read: Sender<MessageIden>,
        mut receiver_restart: watch::Receiver<()>,
    ) -> Result<(), Error> {
        if let Some(client) = &self.client {
            log::trace!("Started initial sync");
            let sync_timeout = self.config.first_sync_timeout_seconds;
            client
                .sync_once(SyncSettings::default().timeout(Duration::from_secs(sync_timeout)))
                .await?;
            log::trace!("Finished initial sync");
            let sender = sender.clone();
            let config = self.config.clone();
            // Ugly workaround for not having a context for notification handlers.
            client
                .register_notification_handler({
                    let sender = sender.clone();
                    move |notification, room, client| {
                        let sender = sender.clone();
                        let config = config.clone();
                        async move {
                            Self::on_notification(notification, room, client, sender, config).await;
                        }
                    }
                })
                .await;

            let client_ref = &client;
            loop {
                tokio::select! {
                    _ = receiver_restart.changed() => {
                        log::trace!("Matrix got request to restart receiving");
                        continue;
                    }
                    _ = {
                        log::trace!("Started sync");
                        let settings = SyncSettings::default().token(client.sync_token().await.unwrap());
                        client.sync_with_callback(settings.clone(), |response| async move {
                            let client = &client_ref;
                            for event in response.to_device.events.iter().filter_map(|e| e.deserialize().ok()) {
                                match event {
                                    AnyToDeviceEvent::KeyVerificationRequest(e) => {
                                        let request = client
                                            .encryption()
                                            .get_verification_request(&e.sender, &e.content.transaction_id)
                                            .await
                                            .expect("Request object wasn't created");

                                        request.accept().await.expect("Can't accept verification request");
                                    }
                                    AnyToDeviceEvent::KeyVerificationStart(e) => {
                                        if let Some(Verification::SasV1(sas)) = client
                                            .encryption()
                                            .get_verification(&e.sender, e.content.transaction_id.as_str())
                                            .await
                                        {
                                            log::trace!(
                                                "Starting verification with {} {}",
                                                &sas.other_device().user_id(),
                                                &sas.other_device().device_id()
                                            );
                                            sas.accept().await.unwrap();
                                        }
                                    }

                                    AnyToDeviceEvent::KeyVerificationKey(e) => {
                                        if let Some(Verification::SasV1(sas)) = client
                                            .encryption()
                                            .get_verification(&e.sender, e.content.transaction_id.as_str())
                                            .await
                                        {
                                            tokio::spawn(wait_for_confirmation(sas));
                                        }
                                    }

                                    AnyToDeviceEvent::KeyVerificationMac(e) => {
                                        if let Some(Verification::SasV1(sas)) = client
                                            .encryption()
                                            .get_verification(&e.sender, e.content.transaction_id.as_str())
                                            .await
                                        {
                                            if sas.is_done() {
                                                let device = sas.other_device();

                                                log::trace!(
                                                    "Successfully verified device {} {} {:?}",
                                                    device.user_id(),
                                                    device.device_id(),
                                                    device.local_trust_state()
                                                );
                                            }
                                        }
                                    }

                                    _ => (),
                                }
                            }
                            LoopCtrl::Continue
                        })
                    } => {
                        // Should never happen
                        break;
                    }
                }
            }

            Ok(())
        } else {
            log::warn!("Matrix client not linked, exiting");
            Ok(())
        }
    }
}

async fn wait_for_confirmation(sas: SasVerification) {
    let emojis = sas.emoji();
    log::trace!("Notifying confirmation with emojis: {:?}", sas.emoji());
    let mut notification_body = "Please confirm the following emojis: \n".to_string();
    if emojis.is_none() {
        sas.cancel().await.unwrap();
        return;
    }
    for emoji in emojis.unwrap() {
        writeln!(
            &mut notification_body,
            "{} ({})",
            emoji.symbol, emoji.description
        )
        .expect("Failed to write to String");
    }
    let verified = confirm_notification("Matrix verification request", &notification_body)
        .await
        .unwrap_or_default();

    if verified {
        sas.confirm().await.unwrap();

        if sas.is_done() {
            let device = sas.other_device();

            log::trace!(
                "Successfully verified device {} {} {:?}",
                device.user_id(),
                device.device_id(),
                device.local_trust_state()
            );
        }
    } else {
        log::trace!("Could not confirm emojis");
        sas.cancel().await.unwrap();
    }
}
