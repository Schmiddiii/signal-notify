use futures::{channel::oneshot, future, pin_mut, StreamExt};
use presage::{
    prelude::{
        content::Reaction,
        proto::{call_message::offer::Type, sync_message::Sent, CallMessage as PresCallMessage},
        Contact, Content, ContentBody, GroupContextV2, GroupMasterKey, ServiceAddress,
        SignalServers, SyncMessage,
    },
    Manager, Registered, SledConfigStore,
};
use tokio::sync::{mpsc::Sender, watch};

use crate::{
    config::SignalConfig,
    message::{Attachment, CallMessage, CallType, Message, MessageIden, Messenger, TextMessage},
    Error,
};

pub struct Signal {
    manager: Option<presage::Manager<SledConfigStore, Registered>>,
    config_store: presage::SledConfigStore,
    config: SignalConfig,
}

impl Signal {
    pub fn new(c: SledConfigStore, config: SignalConfig) -> Self {
        log::debug!("Setup Signal manager.");
        let manager = Manager::load_registered(c.clone()).ok();
        if manager.is_none() {
            log::warn!("Signal manager not yet linked")
        }
        Self {
            config_store: c,
            manager,
            config,
        }
    }

    pub async fn link(&mut self, name: String) -> Result<(), Error> {
        log::debug!("Linking Signal manager.");
        let (tx, rx) = oneshot::channel();

        let (manager, _) = future::join(
            Manager::link_secondary_device(
                self.config_store.clone(),
                SignalServers::Production,
                name,
                tx,
            ),
            async move {
                match rx.await {
                    Ok(url) => qr2term::print_qr(url.to_string()).expect("failed to render qrcode"),
                    Err(e) => println!("Error linking device: {}", e),
                }
            },
        )
        .await;

        self.manager = Some(manager?);

        Ok(())
    }

    async fn group_by_v2(&self, context: Option<GroupContextV2>) -> Option<String> {
        if let Some(k) = context.and_then(|c| c.master_key) {
            self.manager
                .as_ref()
                .expect("Manager not set up")
                .get_group_v2(GroupMasterKey::new(
                    k.try_into().expect("GroupMasterKey to be 32 byte"),
                ))
                .await
                .ok()
                .map(|g| g.title)
        } else {
            None
        }
    }

    fn skip_archived_blocked(&self, address: &ServiceAddress) -> bool {
        let contact = self.sender_contact_by_address(address);
        contact
            .map(|c| {
                (c.blocked && self.config.skip_blocked) || (c.archived && self.config.skip_archived)
            })
            .unwrap_or_default()
    }

    fn sender_contact_by_address(&self, address: &ServiceAddress) -> Option<Contact> {
        let manager = self.manager.as_ref().expect("Manager not set up");
        address
            .uuid
            .and_then(|uuid| manager.get_contact_by_id(uuid).ok().flatten())
    }

    fn sender_by_address(&self, address: &ServiceAddress) -> String {
        let manager = self.manager.as_ref().expect("Manager not set up");
        if address.uuid == Some(manager.uuid()) {
            return crate::YOU.to_owned();
        }
        let sender_contact_name = address.uuid.and_then(|uuid| {
            manager
                .get_contact_by_id(uuid)
                .ok()
                .flatten()
                .map(|c| c.name)
        });
        if let Some(name) = sender_contact_name {
            if name.is_empty() {
                address
                    .phonenumber
                    .as_ref()
                    .map(|p| p.to_string())
                    .unwrap_or_else(|| "Unknown Sender".to_string())
            } else {
                name
            }
        } else {
            address
                .phonenumber
                .as_ref()
                .map(|p| p.to_string())
                .unwrap_or_else(|| "Unknown Sender".to_string())
        }
    }

    pub async fn receive(
        &self,
        sender: Sender<Message>,
        sender_read: Sender<MessageIden>,
        mut receiver_restart: watch::Receiver<()>,
    ) -> Result<(), Error> {
        if let Some(manager) = &self.manager {
            loop {
                log::debug!("Querying messages");
                let messages = manager.receive_messages().await?;
                pin_mut!(messages);
                log::trace!("Querying messages finished");

                loop {
                    tokio::select! {
                        _ = receiver_restart.changed() => {
                            log::trace!("Signal got request to restart receiving");
                            break
                        }
                        Some(Content { metadata, body }) = messages.next() => {
                            match body {
                                ContentBody::CallMessage(PresCallMessage {
                                    offer: Some(offer), ..
                                }) => {
                                    if self.skip_archived_blocked(&metadata.sender) {
                                        continue;
                                    }
                                    sender
                                        .send(Message::Call(CallMessage {
                                            messenger: Messenger::Signal,
                                            timestamp: metadata.timestamp,
                                            sender: self.sender_by_address(&metadata.sender),
                                            sender_id: metadata
                                                .sender
                                                .uuid
                                                .map(|u| u.to_string())
                                                .unwrap_or_else(|| "".to_string()),
                                            r#type: match offer.r#type() {
                                                Type::OfferAudioCall => CallType::Audio,
                                                Type::OfferVideoCall => CallType::Video,
                                            },
                                        }))
                                        .await
                                        .expect("Failed to send message through Signal sender")
                                },
                                ContentBody::DataMessage(message)
                                | ContentBody::SynchronizeMessage(SyncMessage {
                                    sent:
                                        Some(Sent {
                                            message: Some(message),
                                            ..
                                        }),
                                    ..
                                }) => {
                                    if self.skip_archived_blocked(&metadata.sender) {
                                        continue;
                                    }
                                    if message.delete.is_some() {
                                        // skip delete messages
                                        continue;
                                    }

                                    if let Some(Reaction {
                                        remove: Some(true), ..
                                    }) = &message.reaction
                                    {
                                        // skip emoji reaction removals
                                        continue;
                                    }

                                    if (message.body.is_none() || message.body.as_ref().unwrap().is_empty()) && message.attachments.is_empty() && message.reaction.is_none() && message.sticker.is_none() {
                                        // Skip empty messages
                                        continue;
                                    }

                                    log::debug!("Got actual message. Building notification");

                                    sender
                                        .send(Message::Text(TextMessage {
                                            messenger: Messenger::Signal,
                                            timestamp: metadata.timestamp,
                                            sender: self.sender_by_address(&metadata.sender),
                                            sender_id: metadata
                                                .sender
                                                .uuid
                                                .map(|u| u.to_string())
                                                .unwrap_or_else(|| "".to_string()),
                                            group: self.group_by_v2(message.group_v2).await,
                                            body: message.body.clone(),
                                            reaction: message.reaction.and_then(|r| r.emoji),
                                            sticker: message.sticker.is_some(),
                                            attachments: message
                                                .attachments
                                                .iter()
                                                .map(|a| match &a.content_type {
                                                    Some(content_type) => {
                                                        let media_type = &content_type[..6];
                                                        match media_type {
                                                            "audio/" => Attachment::Audio,
                                                            "image/" => Attachment::Image,
                                                            "video/" => Attachment::Video,
                                                            _ => Attachment::File,
                                                        }
                                                    }
                                                    None => Attachment::File,
                                                })
                                                .collect(),
                                        }))
                                        .await
                                        .expect("Failed to send message through Signal sender")
                                }
                                ContentBody::SynchronizeMessage(SyncMessage { read: reads, .. }) => {
                                    for read in &reads {
                                        let ident = MessageIden {
                                            timestamp: read.timestamp.expect("Read timestamp to be set"),
                                            sender: read
                                                .sender_uuid
                                                .as_ref()
                                                .expect("Read Uuid to be valid")
                                                .clone(),
                                        };
                                        sender_read
                                            .send(ident)
                                            .await
                                            .expect("Failed to send message through Signal sender_read");
                                    }
                                }
                                _ => {
                                    log::debug!("This message cannot currently be handled.");
                                }
                            }
                        }
                    }
                }
                log::info!("Messages channel disconnected. Trying to reconnect.");
            }
        } else {
            log::warn!("Signal manager not linked, exiting");
            Ok(())
        }
    }
}
