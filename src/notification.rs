use notify_rust::{Hint, Notification, Timeout};

use crate::{config::Config, feedbackd::Feedbacker, Error};

#[derive(Clone)]
pub struct Notifier {
    config: Config,
    feedbacker: Feedbacker,
}

impl Notifier {
    pub async fn new(config: Config) -> Result<Self, Error> {
        Ok(Self {
            config,
            feedbacker: Feedbacker::new().await?,
        })
    }

    pub async fn send_notification(&self, m: impl IntoNotification) -> Result<(), Error> {
        let mut notification = m.as_notification(&self.config);
        let notif = notification.timeout(match self.config.notification_timeout_seconds {
            i if i < 0 => Timeout::Default,
            0 => Timeout::Never,
            i => Timeout::Milliseconds(
                (i * 1000)
                    .try_into()
                    .expect("Conversion of i64 to u32 for notification timeout"),
            ),
        });
        let _ = notif.show()?;

        if self.config.feedback {
            self.feedbacker.feedback().await?;
        }

        Ok(())
    }
}

pub trait IntoNotification {
    fn as_notification(&self, config: &Config) -> Notification;
}

pub async fn confirm_notification<S: AsRef<str>>(title: S, body: S) -> Result<bool, Error> {
    let (sender, receiver) = tokio::sync::oneshot::channel();
    let not = Notification::new()
        .summary(title.as_ref())
        .body(body.as_ref())
        .action("ok", "Ok")
        .action("cancel", "Cancel")
        .timeout(0)
        .hint(Hint::Resident(true))
        .show()?;
    not.wait_for_action(|action| match action {
            "ok" => {
                sender
                    .send(true)
                    .expect("ConfirmNotification dropped receiver");
            }
            _ => {
                sender
                    .send(false)
                    .expect("ConfirmNotification dropped receiver");
            }
        });
    let result = receiver.await.expect("ConfirmNotification dropped sender");

    Ok(result)
}
