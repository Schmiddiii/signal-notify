use std::collections::HashMap;
use std::sync::{Arc, Mutex};
use std::time::{Duration, SystemTime};
use std::{env, path::PathBuf};

pub use error::Error;
use message::{Message, MessageIden};
use tokio::sync::{mpsc, watch};

use crate::login1::Login1;
use crate::notification::Notifier;
use crate::waked::Waker;

mod config;
mod error;
mod feedbackd;
mod login1;
mod message;
mod notification;
mod waked;

#[cfg(feature = "matrix")]
mod matrix;
#[cfg(feature = "signal")]
mod signal;

#[cfg(feature = "matrix")]
use crate::matrix::Matrix;
#[cfg(feature = "signal")]
use crate::signal::Signal;

const MESSAGE_BOUND: usize = 10;

pub const YOU: &str = "You";

// TODO: Help dependent on compilation?
const HELP: &str = "\
messenger-notify
USAGE:
  messenger-notify [OPTIONS]
OPTIONS:
  -h, --help                        Prints help information.
  --link-signal <device-name>       Link with signal and exit. This will display a QR-Code to scan.
  --link-matrix <user-id>           Link with matrix and exit. This will ask for your password.
";

#[derive(Debug)]
struct Args {
    #[cfg(feature = "signal")]
    signal_device_name: Option<String>,
    #[cfg(feature = "matrix")]
    matrix_user_id: Option<String>,
}

// Source: https://github.com/boxdot/gurk-rs/blob/master/src/main.rs#L77
async fn is_online() -> bool {
    log::trace!("Checking online status");
    tokio::net::TcpStream::connect("detectportal.firefox.com:80")
        .await
        .is_ok()
}

fn parse_args() -> Result<Args, pico_args::Error> {
    let mut pargs = pico_args::Arguments::from_env();

    if pargs.contains(["-h", "--help"]) {
        print!("{}", HELP);
        std::process::exit(0);
    }

    #[cfg(feature = "signal")]
    let signal_device_name = pargs.opt_value_from_str("--link-signal")?;
    #[cfg(feature = "matrix")]
    let matrix_user_id = pargs.opt_value_from_str("--link-matrix")?;

    Ok(Args {
        #[cfg(feature = "signal")]
        signal_device_name,
        #[cfg(feature = "matrix")]
        matrix_user_id,
    })
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    env_logger::init();

    log::trace!("Starting up messenger-notify");

    let args = parse_args()?;

    let db_path = PathBuf::from(
        env::var("MN_DATA_PATH").unwrap_or(
            env::var("XDG_DATA_HOME")
                .map(|s| s + "/messenger-notify/")
                .unwrap_or(env::var("HOME").map(|s| s + "/.local/share/messenger-notify/")?),
        ),
    );
    let config_path = PathBuf::from(
        env::var("SN_CONFIG_PATH").unwrap_or(
            env::var("XDG_CONFIG_HOME")
                .map(|s| s + "/messenger-notify/config.toml")
                .unwrap_or(env::var("HOME").map(|s| s + "/.config/messenger-notify/config.toml")?),
        ),
    );
    log::debug!("Use data directory: {}", db_path.display());
    log::debug!("Use configuration file: {}", config_path.display());
    let config = config::config(config_path)?;
    log::trace!("Got args {:#?}", args);
    log::trace!("Got config {:#?}", config);

    #[cfg(feature = "signal")]
    let signal_path = {
        let mut signal_path = db_path.clone();
        signal_path.push("signal");
        signal_path
    };
    #[cfg(feature = "signal")]
    let signal_config_store = presage::SledConfigStore::new(signal_path)?;

    #[cfg(feature = "matrix")]
    let matrix_path = {
        let mut matrix_path = db_path.clone();
        matrix_path.push("matrix");
        matrix_path
    };

    #[cfg(feature = "signal")]
    if let Some(device_name) = args.signal_device_name {
        let mut signal = Signal::new(signal_config_store, config.signal.clone());
        signal.link(device_name).await?;
        return Ok(());
    }

    #[cfg(feature = "matrix")]
    if let Some(user_id) = args.matrix_user_id {
        let mut matrix = Matrix::new(matrix_path, config.matrix.clone()).await?;
        let password = rpassword::prompt_password("Password: ").expect("Failed to read password");
        matrix.login(user_id, password).await?;
        return Ok(());
    }

    let (restart_send, restart_receive) = watch::channel(());
    if config.wake || config.restart_after_suspend {
        let wake_minutes = Duration::from_secs(config.wake_interval_minutes * 60);
        tokio::spawn(async move {
            let waker = Waker::new().await.ok();
            let login1 = Login1::new().await.expect("Login1 failed to initialize");
            let mut inhibit_fd = if config.wake {
                Some(
                    login1
                        .inhibit("sleep", "messenger-notify", "Schedule wakeup", "delay")
                        .await
                        .expect("Failed to inhibit"),
                )
            } else {
                None
            };
            loop {
                let sleep = login1.receive_sleep().await.expect("Receive sleep failed");
                if sleep {
                    log::trace!("Going to sleep, schedule wakeup");
                    if config.wake {
                        waker
                            .as_ref()
                            .expect("Waked failed to initialize")
                            .schedule_wakeup(
                                SystemTime::now() + wake_minutes - Duration::from_secs(60),
                            )
                            .await
                            .expect("Failed to wait");
                    }
                    if inhibit_fd.is_some() {
                        let fd = inhibit_fd.take();
                        drop(fd);
                    }
                } else {
                    log::trace!("Waking up, restarting messengers");
                    inhibit_fd = if config.wake {
                        Some(
                            login1
                                .inhibit("sleep", "messenger-notify", "Schedule wakeup", "delay")
                                .await
                                .expect("Failed to inhibit"),
                        )
                    } else {
                        None
                    };

                    while !is_online().await {
                        log::trace!("Currently offline, trying again in 10 seconds");
                        tokio::time::sleep(Duration::from_secs(10)).await;
                    }
                    // The services need to be restarted, otherwise they may notice too late that the
                    // connection to websockets is broken.
                    restart_send.send(()).expect("Failed to send restart");
                }
            }
        });
    }

    let (message_send, mut message_receive) = mpsc::channel(MESSAGE_BOUND);
    let (message_read_send, mut message_read_receive) = mpsc::channel(MESSAGE_BOUND);
    let (crash_send, mut crash_receive) = mpsc::channel(1);

    #[cfg(feature = "signal")]
    {
        let signal_send = message_send.clone();
        let crash_send = crash_send.clone();
        let signal_read_send = message_read_send.clone();
        let signal_restart_receive = restart_receive.clone();
        let config = config.signal.clone();
        let _ = std::thread::spawn(move || {
            tokio::runtime::Runtime::new()
                .expect("Failed to setup runtime")
                .block_on(async move {
                    let signal = Signal::new(signal_config_store, config);
                    let result = signal
                        .receive(signal_send, signal_read_send, signal_restart_receive)
                        .await;
                    if let Err(e) = result {
                        crash_send
                            .send(e)
                            .await
                            .expect("Failed to send crash message");
                    }
                });
        });
    }

    #[cfg(feature = "matrix")]
    {
        let matrix_send = message_send.clone();
        let matrix_read_send = message_read_send.clone();
        let matrix_restart_receive = restart_receive.clone();
        let config = config.matrix.clone();
        let _ = std::thread::spawn(move || {
            tokio::runtime::Runtime::new()
                .expect("Failed to setup runtime")
                .block_on(async move {
                    let matrix = Matrix::new(matrix_path, config).await;
                    if let Err(e) = matrix {
                        crash_send
                            .send(e)
                            .await
                            .expect("Failed to send crash message");
                        return;
                    }
                    if let Err(e) = matrix
                        .unwrap()
                        .receive(matrix_send, matrix_read_send, matrix_restart_receive)
                        .await
                    {
                        crash_send
                            .send(e)
                            .await
                            .expect("Failed to send crash message");
                    }
                });
        });
    }

    let notifier = Notifier::new(config.clone()).await?;

    let message_queue: Arc<Mutex<HashMap<MessageIden, Message>>> =
        Arc::new(Mutex::new(HashMap::new()));

    loop {
        tokio::select! {
            Some(m) = message_receive.recv() => {
                if config.ignore_self &&  m.sender() == YOU {
                    log::trace!("Skipping message from self");
                    continue;
                }
                let ident: MessageIden = (&m).into();
                log::trace!("Receiving message with ident {:#?}.", ident);
                let mut queue = message_queue.lock().expect("Poisoned Mutex");
                queue.insert(ident.clone(), m);
                drop(queue);
                let message_queue = message_queue.clone();

                let config = config.clone();
                let notifier = notifier.clone();
                tokio::spawn(async move {
                    tokio::time::sleep(Duration::from_secs(config.read_timeout_seconds)).await;
                    let msg = { message_queue.lock().expect("Poisoned Mutex").remove(&ident) };
                    if let Some(m) = msg {
                        log::trace!("Notifying message with ident {:#?}.", ident);
                        notifier.send_notification(m).await.expect("Failed to send notification");
                    } else {
                        log::trace!("Message already read with timestamp {:#?}.", ident);
                    }
                });
            }
            Some(i) = message_read_receive.recv() => {
                log::trace!("Marking message as read with timestamp {:#?}.", i);
                let mut queue = message_queue.lock().expect("Poisoned Mutex");
                if queue.remove(&i).is_some() {
                    log::trace!("Successfully removed message");
                } else {
                    log::trace!("Message already displayed");
                }
            }
            Some(e) = crash_receive.recv() => {
                log::error!("Something crashed: {}", e);
                if config.go_down_on_crash {
                    break
                }
            },
            else => break,
        }
    }

    log::trace!("messenger-notify now exiting");

    Ok(())
}
