use std::{fs::File, io::Read, path::Path};

use serde::Deserialize;

use crate::Error;

fn fifteen() -> u64 {
    15
}
fn one() -> u64 {
    1
}
fn minusone() -> i64 {
    -1
}
fn threehundred() -> u64 {
    300
}
fn r#true() -> bool {
    true
}

#[derive(Deserialize, Debug, Clone)]
pub struct Config {
    #[serde(default)]
    pub wake: bool,
    #[serde(default = "fifteen")]
    pub wake_interval_minutes: u64,
    #[serde(default = "r#true")]
    pub restart_after_suspend: bool,
    #[serde(default = "r#true")]
    pub feedback: bool,
    #[serde(default = "r#true")]
    pub ignore_self: bool,
    #[serde(default = "one")]
    pub read_timeout_seconds: u64,
    #[serde(default = "minusone")]
    pub notification_timeout_seconds: i64,
    #[serde(default = "r#true")]
    pub go_down_on_crash: bool,
    #[serde(default)]
    pub signal: SignalConfig,
    #[serde(default)]
    pub matrix: MatrixConfig,
}

#[derive(Deserialize, Debug, Default, Clone)]
pub struct SignalConfig {
    #[serde(default)]
    pub icon: String,
    #[serde(default = "r#true")]
    pub skip_archived: bool,
    #[serde(default = "r#true")]
    pub skip_blocked: bool,
}

#[derive(Deserialize, Debug, Default, Clone)]
pub struct MatrixConfig {
    #[serde(default)]
    pub icon: String,
    #[serde(default)]
    pub only_rooms: Vec<String>,
    #[serde(default)]
    pub skip_rooms: Vec<String>,
    #[serde(default = "r#true")]
    pub all_mentions: bool,
    #[serde(default = "r#true")]
    pub all_private: bool,
    #[serde(default = "threehundred")]
    pub first_sync_timeout_seconds: u64,
}

pub fn config<P: AsRef<Path>>(p: P) -> Result<Config, Error> {
    if p.as_ref().exists() {
        let mut content = String::new();
        let mut file = File::open(p)?;
        file.read_to_string(&mut content)?;
        Ok(toml::from_str(&content)?)
    } else {
        Ok(Config::default())
    }
}

impl Default for Config {
    fn default() -> Self {
        Self {
            wake: false,
            wake_interval_minutes: 15,
            restart_after_suspend: true,
            feedback: false,
            ignore_self: false,
            read_timeout_seconds: 10,
            notification_timeout_seconds: -1,
            go_down_on_crash: true,
            signal: SignalConfig::default(),
            matrix: MatrixConfig::default(),
        }
    }
}
