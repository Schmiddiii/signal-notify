use futures::StreamExt;
use zbus::{dbus_proxy, zvariant::OwnedFd, Connection};

#[dbus_proxy(
    interface = "org.freedesktop.login1.Manager",
    default_service = "org.freedesktop.login1",
    default_path = "/org/freedesktop/login1"
)]
trait Login1 {
    fn inhibit(&self, what: &str, who: &str, why: &str, mode: &str) -> zbus::Result<OwnedFd>;

    #[dbus_proxy(signal)]
    fn prepare_for_sleep(&self, arg1: bool) -> fdo::Result<()>;
}

pub struct Login1 {
    connection: Connection,
}

impl Login1 {
    pub async fn new() -> Result<Self, zbus::Error> {
        Ok(Self {
            connection: Connection::system().await?,
        })
    }

    pub async fn inhibit(
        &self,
        what: &str,
        who: &str,
        why: &str,
        mode: &str,
    ) -> Result<OwnedFd, zbus::Error> {
        let proxy = Login1Proxy::new(&self.connection).await?;
        proxy.inhibit(what, who, why, mode).await
    }

    pub async fn receive_sleep(&self) -> Result<bool, zbus::Error> {
        let proxy = Login1Proxy::new(&self.connection).await?;
        proxy
            .receive_prepare_for_sleep()
            .await?
            .next()
            .await
            .expect("Signal never received")
            .args()
            .map(|a| *a.arg1())
    }
}
